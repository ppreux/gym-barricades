/*
 * barricade.h

   Version 0.1 (20/3/2020)

   Developed by Philippe.Preux@univ-lille.fr

   This is LGPL software.

 */

#ifndef VOISINAGES_H_ALREADY_INCLUDED

#define VOISINAGES_H_ALREADY_INCLUDED
#define N_VOISINAGES 699
extern int voisinages [N_VOISINAGES] [6];
extern const int voisinages2 [N_VOISINAGES] [7];

#endif
