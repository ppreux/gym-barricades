import random
from gym_barricades import Barricade

random.seed (123456)

def play_a_random_game (max_plays = 100000):
    noms_couleur = ['red', 'green', 'yellow', 'blue']
    game = Barricade ()
    o = game.get_observation ()
    fini = False
    couleur = 0
    compteur = 0
    while (not fini):
        de = random.randint (1, 6)
        possible_moves = game.get_actions (o [noms_couleur [couleur]], de)
        if (len (possible_moves) > 0):
            idx = 0
            i_start = random.randint (0, len (possible_moves) - 1)
            for start in possible_moves:
                if i_start == 0:
                    idx = start
                    break
                i_start -= 1
            m = random.randint (0, len (possible_moves [idx]) - 1)
            end_position = possible_moves [idx] [m]. end_position
            occupied_cells = list (o['red'] + o['yellow'] + o['green'] + o['blue'])
            occupied_cells.append (end_position)
            occupied_cells.remove (idx)
            tentative_cell = random.randint (1, 103)
            while (tentative_cell in occupied_cells):
                tentative_cell = random.randint (1, 103)
            #print (str (compteur) + ": " + str (idx) + ", " + str (end_position) + ", " + str (tentative_cell))
            o, r, fini = game. step (idx, end_position, tentative_cell)
            if (fini):
                print (noms_couleur [couleur] + " wins after " + str (compteur) + " plays.")
                game.render ()
            if couleur == 3:
                couleur = 0
            else:
                couleur += 1
        '''
        else:
            print (str (compteur) + ": " + noms_couleur [couleur] + " can not play (" + str (de) + ")")
            game.render ()
        '''
        if compteur == max_plays:
            print ("I stop")
            game.render ()
            break
        compteur += 1
    
play_a_random_game ()
