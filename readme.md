# What is `gym-barricades`?

This is a gym-like environment to play the game named barricades.

If you wonder, gym environments are meant to be generic and extremly simple Python environments to let a software program/agent interact with an envirnment in which it has to perform a certain task. gym environments are widely used in the reinforcement learning community. gym environments are currently a de facto standard.

# What is barricades?

Barricades is yet an other board game. Each payer has a couple of pawns. They pay in turn, and they compete to be the first to reach a goal cell. Movements are dictated by the roll of a dice. At each turn, a player moves one of her pawn, struggling to reach the goal, and to set traps to his adversaries: either by throwing one of its pawn back at its starting location, or by setting barricade on their way. The rules of the game are provided in the accompanying documentation (file named `gym-barricades.pdf`).

This figure shows an on-going game:

<img alt="Picture of a barricades game." src="./an-on-going-Barricades-game.jpg" width="50%" title="An on-going barricades game." />

Human beings can use this software program to play the game, but it is not meant for that. It is absolutly not user-friendly, though one may develop an interface to make it user-friendly. The real purpose of this software is to be used to develop a program that plays this game, or learns to play it.

In terms of software, it is extremly simple to interact with in python.

# Content of this repo

This directory contains:
* the C code for the simulator (files `*.c` and `*.h`, and the `Makefile`)
* a compiled library for Linux systems: `libbarricades.so`
* the Python interface à la gym: `gym_barricades.py`
* a how-to example: `random_play.py`
* documentation: `gym-barricades.pdf`
* this file: `readme.md`

# Using gym-barricades

The code has been develpped in a very standard Ubuntu 20.04 distribution (we use gcc). To use it: either you can use the compiler library directly, ot you need a C compiler. In either case, you probably need python3, even though you can directly write your code in C, or C++, linking with the libray `libbarricades.so`.

To use this library:
```
git clone git@gitlab.inria.fr:ppreux/gym-barricades.git
cd gym-barricades
```
* if you work on an Ubuntu 20.04 system, and you want to use Python: in Python, you simply `from gym_barricades import Barricade` and then follow the documentation `gym-barricades.pdf`.
* if you work on an Ubuntu 20.04 system, and you want to re-compile the sources: you simply type `make` in a shell, and you are back in the previous situation.
* if you are not working with an Ubuntu 20.04 but with an other Linux (a standard one) distribution: it is very likely that the previous case will work for you.

# Development of gym-barricades

The core of the simulator was developped in C by Philippe Preux (Université de Lille, France) in 2019. Julien Teigny (Inria, France) developed the interface between C and Python, and he also spotted and fixed an important bug. Is is finally put online in December 2021.

# Disclaimer and licencing

This software is available under MIT Licence.
