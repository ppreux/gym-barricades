/*
 * vars.c

   Version 0.1 (20/3/2020)

   Developed by Philippe.Preux@univ-lille.fr

   This is LGPL software.

   This file is part of the barricade project.
   It declares the variables shared between C source files.
 */

#include <stddef.h>

#include "barricades.h"
#include "voisinages.h"

P_The_board tb;
T_mp mp [MAX_MVT_POSSIBLES];
int nb_mp = 0;
size_t ideb = 0;

int debut_voisinage [NB_CASES + 1] = { 0, 0, 2, 4, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 41, 43, 45, 47, 49, 51, 53, 55, 58, 61, 65, 69, 73, 77, 81, 85, 89, 92, 95, 97, 99, 101, 107, 112, 118, 124, 130, 135, 142, 149, 154, 161, 167, 173, 177, 183, 189, 196, 201, 208, 215, 221, 229, 236, 246, 254, 264, 272, 282, 290, 300, 307, 315, 321, 329, 341, 353, 361, 365, 373, 380, 391, 399, 412, 423, 438, 448, 463, 474, 487, 495, 506, 513, 521, 525, 530, 539, 551, 560, 565, 569, 575, 579, 586, 593, 602, 608, 618, 626, 636, 642, 651, 658, 665, 669, 675, 679, 683, 689, 695, N_VOISINAGES };
