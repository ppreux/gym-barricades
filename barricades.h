/*
 * barricade.h

   Version 0.1 (20/3/2020)

   Developed by Philippe.Preux@univ-lille.fr

   This is LGPL software.

 */

#ifndef __BARRICADES_H_ALREADY_INCLUDED__
#define __BARRICADES_H_ALREADY_INCLUDED__

#define MY_NAME program_invocation_short_name
#define VERSION "0.1"

#define NB_CASES 116
#define NB_LINES 16
#define MAX_LGTH 21
#define NB_PIONS_PAR_COULEUR 5

#define BARRICADE 'r'
#define OBJECTIF 'G'
#define CASE_ROUGES 112
#define CASE_VERTS 113
#define CASE_JAUNES 114
#define CASE_BLEUS 115
#define CASE_FINALE 0

typedef struct {
  size_t num;
  size_t i, j;
  char contenu;
  int voisines [4];
} Case, *P_Case;
typedef struct {
  P_Case lc;
  size_t nb_cases;
} The_board, *P_The_board;
extern P_The_board tb;

#define MAX_MVT_POSSIBLES 20
typedef struct {
  int cell_arrivee;
  char prise_adversaire;
  int barricade;
  int goal;
} T_mp;

extern T_mp mp [MAX_MVT_POSSIBLES];
extern int nb_mp;

extern size_t ideb;
extern int debut_voisinage [NB_CASES + 1];

extern void print_board (void);
extern P_The_board code_board (void);
extern void possible_moves (const int, const int);
extern int move (const int, const int, const int);
extern void perform_a_random_test (void);
extern void init_game (void);


extern int * get_cases_des_rouges(void);
extern int * get_cases_des_verts(void);
extern int * get_cases_des_jaunes(void);
extern int * get_cases_des_bleus(void);
extern int * get_cases_des_barricades(void);


#endif
