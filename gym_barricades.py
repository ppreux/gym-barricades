"""
***********************************************                                       
Objectif  : Facade Python -> C for the game 'barricade' (barricade.so)
Auteur    : Inria (Julien Teigny)               
Date      : 15/05/2020
***********************************************                       
"""

import gym
from ctypes import *

#Loading the shared library of the C code
barricade_so = "./libbarricades.so"
my_facade = CDLL(barricade_so)

 
class Move(Structure):
    """Transfert object for available move (T_mp) between C and Python"""
    _fields_ = [("end_position", c_int), ("remove_adversary_pawn", c_char), ("barricade", c_int), ("goal", c_int)]


class Barricade(gym.Env):
    
    metadata = {'render.modes': ['human']}
 
    def __init__(self):
        """
        Facade to init the game in the C code 
        """            
        my_facade.facade_reset_game()
  
    def reset(self):
        """
        Gym reset method
        Facade to reinit the board game to the initial position in the C code
        """
        my_facade.facade_reset_game()
 
    def get_observation(self):
        """
        Gym get_observation method
        Returns the observation about the current state of the game
        :return: observation : dictionary about pawn positions (red,green,yellow,blue,barricade)
        """
        #Get current board game information: bgi
        bgi = (c_int*31)()
        my_facade.facade_get_board_game_information(bgi) 
        dic_observation = {
            "red":(bgi[0],bgi[1],bgi[2],bgi[3],bgi[4]),
            "green":(bgi[5],bgi[6],bgi[7],bgi[8],bgi[9]),
            "yellow":(bgi[10],bgi[11],bgi[12],bgi[13],bgi[14]),
            "blue":(bgi[15],bgi[16],bgi[17],bgi[18],bgi[19]),
            "barricade":(bgi[20],bgi[21],bgi[22],bgi[23],bgi[24],bgi[25],bgi[26],bgi[27],bgi[28],bgi[29],bgi[30]),
            }
        return dic_observation 
         
    def get_actions(self,current_positions,dice_value):
        """
        Method to get available move
        Compute the available move from the dice value and the 5 pawns position.
        :param current_positions: tuples --> current pawn positions
        :param dice_value: int --> current dice value [1-6]
        :return: available_moves
        """
        dic_available_moves_by_starting_position = {}
        
        for current_pawn in current_positions:
            #if the current_pawn is in the dictionnay, no need to compute again
            if (not current_pawn in dic_available_moves_by_starting_position):
                #get the number of available moves by pawn (for this dice_value)
                my_facade.facade_update_available_move(current_pawn,dice_value)
                number_of_available_move = my_facade.facade_get_nb_available_moves()
                                            
                #Get available moves
                available_moves = (Move*number_of_available_move)()
                my_facade.facade_get_available_moves(available_moves)

                #Check if there is 1 move or + (to avoid empty value in the dictionary)
                if (len(available_moves)>0):
                    dic_available_moves_by_starting_position[current_pawn] = available_moves
            
        return dic_available_moves_by_starting_position


    def step(self, starting_position, ending_position, barricade_new_position):
        """
        Gym step method
        Play the move in the 'which_move' index from available_moves (coups_possibles) and remember it as the 'self.nb_move' move of the game.
        :param starting_position: int --> initial position of the current move
        :param ending_position: int --> final position of the current move
        :param barricade_new_position: int --> new position of the barricade to move (the final position should be a barricade)
        :return: observation, reward, done
        """
        my_facade.facade_step.argtypes = [c_int,c_int,c_int]
        success = my_facade.facade_step(starting_position, ending_position, barricade_new_position)
        # PP : faudrait que step renvoie un diagnostic qd le mvt n'est pas possible.
        observation = self.get_observation()
        done = bool(my_facade.facade_game_done()  )
            
        #reward: choose how to compute the reward (maybe give some reward when we move barricade or reset an other player?)
        #currently, reward is 0, except if the game is done (by the last step)
        reward = 0
        if(done):
            reward= 1
        return observation, reward, done


    def render(self, mode='human'):
        """
        Display the current board game
        """        
        my_facade.facade_print_game ()
        
        
        
        
