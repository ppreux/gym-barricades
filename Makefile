libbarricade.so: Makefile voisinages.c voisinages.h barricades.c barricades.h vars.c
	gcc -c -fPIC voisinages.c
	gcc -c -fPIC vars.c
	gcc -c -fPIC barricades.c
	gcc -Wall -fPIC -shared -o libbarricades.so barricades.o vars.o voisinages.o /usr/lib/x86_64-linux-gnu/libgsl.a

check: libbarricades.so
	@python3 random_play.py | diff random_play_output -

clean:
	@rm -rf __pycache__ libbarricades.so *.o
