/*
 * barricade.c

   Version 0.2 (26/12/2021)

   Developed by Philippe.Preux@univ-lille.fr

   This is LGPL software.

   This file is part of the barricade project.
   It contains the simulator of the game.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#include <errno.h>

#include "voisinages.h"
#include "barricades.h"

#define bit64

static const char board [NB_LINES][MAX_LGTH] = {
  "",         /* indice cellule la plus à gauche */
  "         G",          /*  0 */
  " ........r........",  /*  1 */
  " .               .",  /* 18 */
  " ........r........ ", /* 20 */
  "         r         ", /* 37 */
  "       ..r..       ", /* 38 */
  "       .   .       ", /* 43 */
  "     ..r...r..",      /* 45 */
  "     .       .",      /* 54 */
  "   .............",    /* 56 */
  "   .   .   .   .",    /* 69 */
  " r...r...r...r...r",  /* 73 */
  " .   .   .   .   .",  /* 90 */
  " .................",  /* 95 */
  "   R   V   J   B"     /* 112 */
};

#define DOESnotEXIST 1000

typedef char Couleur; /* \in { 'R', 'V', 'J', 'B' } */

#define MaxMvt 20
#define StackDepth 30

static int cases_des_rouges [NB_PIONS_PAR_COULEUR];
static int cases_des_verts [NB_PIONS_PAR_COULEUR];
static int cases_des_jaunes [NB_PIONS_PAR_COULEUR];
static int cases_des_bleus [NB_PIONS_PAR_COULEUR];

static int which_is_there (const int cell, const int *locations)
{
  for (size_t i = 0; i < 5; i ++)
    if (locations [i] == cell) return i;
  return -1;
}

/*static int cell_objectif = 0;*/
#define N_BARRICADES 11
static int cases_des_barricades [N_BARRICADES] = {9, 28, 37, 40, 47, 51,
						  73, 77, 81, 85, 89 };

static int which_barricade_is_there (const int cell)
{
  for (size_t i = 0; i < N_BARRICADES; i ++)
    if (cases_des_barricades [i] == cell) return i;
  return -1;
}

void print_board (void)
{
  static char displayed_board [NB_LINES][MAX_LGTH];
  for (size_t i = 0; i < NB_LINES; i++) {
    for (size_t j = 0; j < MAX_LGTH - 1; j++)
      displayed_board [i] [j] = ' ';
    displayed_board [i] [MAX_LGTH - 2] = '\n';
    displayed_board [i] [MAX_LGTH - 1] = '\0';
  }

  for (size_t cell = 0; cell < NB_CASES; cell++)
    displayed_board [tb-> lc [cell]. i] [tb-> lc [cell]. j] =
      tb-> lc [cell]. contenu;

  for (size_t i = 0; i < NB_LINES; i++)
    printf ("%s", displayed_board [i]);
}

size_t which_case (const P_The_board tb, const size_t i, const size_t j)
{
  for (size_t idx = 0; idx < tb-> nb_cases; idx ++)
    if ((tb-> lc [idx]. i == i) && (tb-> lc [idx]. j == j))
      return (idx);
  fprintf (stderr, "exiting: %zd %zd, %s\n %s\n %s (%c)\n", i, j,
	   board [i-1], board [i], board [i+1], board [i][j]);
  exit (EXIT_FAILURE);
}

P_The_board code_board (void)
{
  int count = 0;
  for (size_t i = 0; i < NB_LINES; i++) {
    for (size_t j = 0; j < strlen (board [i]); j ++)
      if (isgraph (board [i] [j])) count ++;
  }
  P_Case les_cases = malloc (sizeof (Case) * count);
  count = 0;
  int count_barricades = 0;
  for (size_t i = 0; i < NB_LINES; i++)
    for (size_t j = 0; j < strlen (board [i]); j ++)
      if (isgraph (board [i] [j])) {
	les_cases [count]. num = count;
	les_cases [count]. i = i;
	les_cases [count]. j = j;
	les_cases [count]. contenu = board [i] [j];
	if (board [i] [j] == 'r')
	  cases_des_barricades [count_barricades ++] = count;
	count ++;
      }
  if (count_barricades != N_BARRICADES) {
    fprintf (stderr, "%s: expect %d barricades, but read %d.\n",
	     MY_NAME, N_BARRICADES, count_barricades);
    exit (EXIT_FAILURE);
  }
  if (count != NB_CASES) {
    fprintf (stderr, "%s: expect %d cells, but read %d.\n",
	     MY_NAME, NB_CASES, count);
    exit (EXIT_FAILURE);
  }

  P_The_board tb = malloc (sizeof (The_board));
  tb-> nb_cases = count;
  tb-> lc = les_cases;
  for (size_t i = 0; i < NB_LINES; i++)
    for (size_t j = 0; j < strlen (board [i]); j ++) {
      if (isgraph (board [i][j])) {
	size_t idx = which_case (tb, i, j);
	for (size_t k = 0; k < 4; k ++)
	  les_cases [idx]. voisines [k] = DOESnotEXIST;
	if (isgraph (board [i][j-1]))
	  les_cases [idx]. voisines [0] = which_case (tb, i, j - 1);
	if (isgraph (board [i][j+1]))
	  les_cases [idx]. voisines [2] = which_case (tb, i, j + 1);
	if (isgraph (board [i-1][j]))
	  les_cases [idx]. voisines [1] = which_case (tb, i - 1, j);
	if ((i < NB_LINES - 1) && (isgraph (board [i+1][j])))
	  les_cases [idx]. voisines [3] = which_case (tb, i + 1, j);
      }
    }
  return tb;
}

void init_game (void)
{
  for (int i = 0; i < N_VOISINAGES; i ++)
    memcpy (voisinages [i], & (voisinages2 [i] [1]), sizeof (int) * 6);
  for (int i = 0; i < NB_PIONS_PAR_COULEUR; i ++) {
    cases_des_rouges [i] = CASE_ROUGES;
    cases_des_verts [i] = CASE_VERTS;
    cases_des_bleus [i] = CASE_BLEUS;
    cases_des_jaunes [i] = CASE_JAUNES;
  }
}

void possible_moves (const int cell, const int de)
{
  nb_mp = 0;
  if ((de < 0) || (de > 6)) return;
  if ((tb-> lc [cell]. contenu != 'R') && (tb-> lc [cell]. contenu != 'V') &&
      (tb-> lc [cell]. contenu != 'J') && (tb-> lc [cell]. contenu != 'B'))
    return;
  for (int i = debut_voisinage [cell]; i < debut_voisinage [cell + 1]; i ++) {
    int empty = 1; /* le chemin entre case initiale et case finale est-il vide ? : sinon, le mvt est impossible */
    for (int j = 0; j < de - 1; j ++)
      empty &= tb-> lc [voisinages [i] [j]]. contenu == '.';
    if (empty) {
      int not_yet_in = 1; /* est-ce un mvt différent de ceux déjà enregistrés ?
                             Si non, on n'en tiendra pas compte. */
      int cell_arrivee = voisinages [i] [de - 1];
      for (int k = 0; k < nb_mp; k ++)
	not_yet_in &= mp [k]. cell_arrivee != cell_arrivee;
      if (not_yet_in) { /* c'est une nouvelle possibilité : on la stocke */
	if (tb-> lc [cell_arrivee]. contenu == '.') {
	  mp [nb_mp]. cell_arrivee = cell_arrivee;
	  mp [nb_mp]. prise_adversaire = '\000';
	  mp [nb_mp]. barricade = 0;
	  mp [nb_mp]. goal = 0;
	  nb_mp ++;
	} else if (tb-> lc [cell_arrivee]. contenu == 'r') {
	  mp [nb_mp]. cell_arrivee = cell_arrivee;
	  mp [nb_mp]. prise_adversaire = '\000';
	  mp [nb_mp]. barricade = 1;
	  mp [nb_mp]. goal = 0;
	  nb_mp ++;
	} else if (tb-> lc [cell_arrivee]. contenu == 'G') {
	  mp [nb_mp]. cell_arrivee = cell_arrivee;
	  mp [nb_mp]. prise_adversaire = '\000';
	  mp [nb_mp]. barricade = 0;
	  mp [nb_mp]. goal = 1;
	  nb_mp ++;
	} else if (tb-> lc [cell_arrivee]. contenu != tb-> lc [cell]. contenu) {
	  mp [nb_mp]. cell_arrivee = cell_arrivee;
	  mp [nb_mp]. prise_adversaire = tb-> lc [cell_arrivee]. contenu;
	  mp [nb_mp]. barricade = 0;
	  mp [nb_mp]. goal = 0;
	  nb_mp ++;
	}
      }
    }
  }
}

/*
 * Déplacement la pièce située sur la case numéro "cell" vers la case
 * numéro "cell_after". Dans le cas où cette case contient une barricade,
 * on la place ensuite sur la case numéro "extra".
 * Le paramètre "extra" n'est utilisé QUE SI la case numéro "cell_after" 
 * contient une barricade.
 */
int move (const int cell, const int cell_after, const int extra)
{
  char joueur_courant = tb-> lc [cell]. contenu;
  if ((joueur_courant != 'R') && (joueur_courant != 'J') && (joueur_courant != 'V') && (joueur_courant != 'B')) {
    printf ("case de départ vide (%d).\n", cell);
    return 0;
  } else if (tb-> lc [cell_after]. contenu == joueur_courant /*tb-> lc [cell]. contenu*/) {
    /* un pion de couleur C veut jouer sur une case contnant déjà un pion de couleur C : c'est interdit. */
    printf ("case d'arrivée inappropriée (%d %d).\n", cell, cell_after);
    return 0;
  } else {
    /* on aiguille selon le contenu de la case où l'on joue */
    switch (tb-> lc [cell_after]. contenu) {
    case '.': break; /* case vide => rien à faire */
    case 'G': /* case finale => on a fini, on gagne */
      /* printf ("you win!\n"); */ break;
    case 'r': { /* on joue une case où se trouve une barricade */
      int w = which_barricade_is_there (cell_after);
      /* on bouge la barricade sur la case numéro "extra" */
      cases_des_barricades [w] = extra;
      /* orig : tb-> lc [extra]. contenu = 'r'; */
      /* bug corrigé par JT */
      if (cell != extra) tb-> lc [extra]. contenu = 'r';
      break;
    }
    default: { /* la case "cell_after" contient un pion d'une autre couleur que celle du joueur courant (ce cas est traité dès le début de cette fct. */
      /* => on détermine qui est sur la case où l'on joue et on le remet dans son enclos. */
      int w = -1;
      if ((w = which_is_there (cell_after, cases_des_rouges)) >= 0) {
	cases_des_rouges [w] = CASE_ROUGES;
	tb-> lc [CASE_ROUGES]. contenu = 'R';
      } else if ((w = which_is_there (cell_after, cases_des_verts)) >= 0) {
	cases_des_verts [w] = CASE_VERTS;
	tb-> lc [CASE_VERTS]. contenu = 'V';
      } else if ((w = which_is_there (cell_after, cases_des_jaunes)) >= 0) {
	cases_des_jaunes [w] = CASE_JAUNES;
	tb-> lc [CASE_JAUNES]. contenu = 'J';
      } else if ((w = which_is_there (cell_after, cases_des_bleus)) >= 0) {
	cases_des_bleus [w] = CASE_BLEUS;
	tb-> lc [CASE_BLEUS]. contenu = 'B';
      }
      break;
    }
    }
    int w = -1;
    switch (joueur_courant /*tb-> lc [cell]. contenu*/) {
    case 'R':
      w = which_is_there (cell, cases_des_rouges);
      cases_des_rouges [w] = cell_after;
      break;
    case 'V':
      w = which_is_there (cell, cases_des_verts);
      cases_des_verts [w] = cell_after;
      break;
    case 'J':
      w = which_is_there (cell, cases_des_jaunes);
      cases_des_jaunes [w] = cell_after;
      break;
    case 'B':
      w = which_is_there (cell, cases_des_bleus);
      cases_des_bleus [w] = cell_after;
      break;
    }
    tb-> lc [cell_after]. contenu = joueur_courant /*tb-> lc [cell]. contenu*/;

    /* bug corrigé par JT */
    /* mise à jour du contenu de la case libérée */
    if (cell == extra) tb-> lc [extra]. contenu = 'r';
    if( (cell < 112) && (cell != extra)) tb-> lc [cell]. contenu = '.';
    else { /* on joue un pion qui était dans son enclos. 
	      On vérifie si cet enclos est vide ou s'il contient
	      encore au moins un pion. */
      switch (joueur_courant /*tb-> lc [cell]. contenu*/) {
      case 'R':
	w = which_is_there (CASE_ROUGES, cases_des_rouges);
	if (w < 0) tb-> lc [CASE_ROUGES]. contenu = '.'; /* enclos rouge vide */
	break;
      case 'V':
	w = which_is_there (CASE_VERTS, cases_des_verts);
	if (w < 0) tb-> lc [CASE_VERTS]. contenu = '.'; /* enclos vert vide */
	break;
      case 'J':
	w = which_is_there (CASE_JAUNES, cases_des_jaunes);
	if (w < 0) tb-> lc [CASE_JAUNES]. contenu = '.'; /* enclos jaune vide */
	break;
      case 'B':
	w = which_is_there (CASE_BLEUS, cases_des_bleus);
	if (w < 0) tb-> lc [CASE_BLEUS]. contenu = '.'; /* enclos bleu vide */
	break;
      }
    }	
  }
  return 1;
}






int * get_cases_des_rouges(void){
  return cases_des_rouges;
}

int * get_cases_des_verts(void){
  return cases_des_verts;
}

int * get_cases_des_jaunes(void){
  return cases_des_jaunes;
}

int * get_cases_des_bleus(void){
  return cases_des_bleus;
}

int * get_cases_des_barricades(void){
  return cases_des_barricades;
}

/* extern functions use as facade */
extern void facade_reset_game(void)
{
  tb = code_board ();
  init_game ();
}

extern int facade_step(int starting_position,int ending_position,int barricade_new_position)
{
  return move (starting_position, ending_position, barricade_new_position);
}

extern void facade_print_game(void)
{
  print_board ();
}

extern int facade_game_done (void)
{
  if (tb-> lc [0].contenu != 'G') return 1;
  else return 0;
}

extern void facade_update_available_move (int cell, int de)
{
  possible_moves (cell, de);
}

extern int facade_get_nb_available_moves (void)
{
  return nb_mp;
}

extern void facade_get_available_moves (T_mp available_moves [])
{  
  for (int i = 0; i < nb_mp; i++)
    available_moves [i] = mp [i];
}

extern void facade_get_board_game_information(int board_game_information[])
{  
  
  int *gcr;
  int *gcv;
  int *gcj;
  int *gcb;
  int *gcbarricade;

  gcr = get_cases_des_rouges();
  gcv = get_cases_des_verts();
  gcj = get_cases_des_jaunes();
  gcb = get_cases_des_bleus();
  gcbarricade = get_cases_des_barricades();

  for(int i = 0; i<5; i++){
    board_game_information[0+i] = *(gcr+i);
    board_game_information[5+i] = *(gcv+i);
    board_game_information[10+i] = *(gcj+i);
    board_game_information[15+i] = *(gcb+i);
  }
  for(int i = 0; i<11; i++){
    board_game_information[20+i] = *(gcbarricade+i);
  }
}
/* End of extern functions use as facade */

